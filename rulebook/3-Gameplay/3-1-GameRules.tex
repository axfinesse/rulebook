%! TEX root = ../rulebook.tex

\subsection{Game rules}
\label{sec:game rules}
\begin{deepEnumerate}
	\item This game being modeled after real baseball, any situation or definition not covered under the rules in this section will default to MLB rules and procedures.
	\item Pre-game procedure
	\begin{deepEnumerate}
		\item Umpires 
		\begin{deepEnumerate}
			\item Umpires are designated by the Ump Warden, or the League Operation Managers in case the Ump Warden is unavailable, prior to the start of each session.
			\item Umpires are responsible for setting up the game thread, pinging pitchers and batters for numbers, and	keeping track of game state.
			\item When assigned, umpires are listed on the schedule tab of the rosters sheet.
		\end{deepEnumerate}
		\item Lineups
		\begin{deepEnumerate}
			\item Lineups may be sent to the Ump Warden at any time. They will be directed to the appropriate ump by the Ump Warden.
			\item GMs may designate a “default lineup” for umps to use if a lineup is not submitted within the first 3 hours of the beginning of a session.
			\begin{deepEnumerate}
				\item If a player on a team’s default lineup is traded or released, the default lineup must be adjusted before the release or trade is approved.
				\item If a team has not set a default lineup and that team has not submitted a lineup within the first 3 hours of the beginning of a session, the League
				Operation Managers reserve the right to create a lineup in their place.
				\item If a GM has not submitted a lineup for a game and the session has begun, 
				\label{sec:captain lineups}
				team captains can submit an emergency lineup within the first 3 hours of the beginning of a session to be used in place of the default lineup.
				\begin{deepEnumerate}
					\item An ump may ask a team captain for an emergency lineup during this window.
				\end{deepEnumerate}
			\end{deepEnumerate}
		\end{deepEnumerate}
	\end{deepEnumerate}
	\item Game procedure
	\begin{deepEnumerate}
		\item Games last 6 innings in regulation time. If the game is tied after six innings, extra innings will be played to determine the winner. 
		See \hyperref[sec:extra innings]{Extra innings}.
		\item As soon as a pitch is needed, the umpire shall notify the pitcher either via Discord, Reddit message, or Reddit chat.
		\begin{deepEnumerate}
			\item Stating the result of the previous at-bat to the pitcher via Discord or Reddit is considered notification of a new pitch being needed.
			\item  In the case of a “list pitcher” (more than one pitch at a time) the umpire MUST clearly request a new pitch or list
			 of pitches once their current list has been exhausted. This is the only case in which stating the result of the previous
			 at bat is not considered notification of a new pitch being needed.
		\end{deepEnumerate}
		\item When a pitch number is submitted, the umpire shall post a top-level comment on the game thread signaling the batter's plate appearance. 
		This must include the player's username as a mention so they get a notification. The umpire is also recommended to mention the player in the Discord if applicable, 
		but this should be seen as a courtesy and is not required.
		\item The player then replies to the top-level comment with their swing. There must be a clear number to be used as the swing, 
		but may optionally include roleplay or other commentary.
		\begin{deepEnumerate}
			\item If multiple numbers are included in the writeup, the batter must make clear which one is the swing. If there is no clear swing, the umpire will use 
			the last number in the swing comment. To ensure the correct swing is used, batters should include it at the very end of the comment, e.g. “Swing: 123”.
			\item A pitcher may change their pitch at any time prior to the batter swinging. The pitch last submitted prior to the swing will be used.
			If it is impossible to determine whether a pitch was changed before or after the swing was submitted, the pitch change shall not be used.
		\end{deepEnumerate}
		\item The umpire shall post the result of the plate appearance as a reply to the player's swing.
		\item Instances of communicating pitch or swing numbers, real or fake, 
		between the current pitcher and batter constitutes unsportsmanlike conduct and will be met with a suspension.
		\item Umpire mistakes
		\begin{deepEnumerate}
			\item In any case of an umpire misapplying rules, general procedure shall be:
			\begin{deepEnumerate}
				\item Reversal of the play if another batter has not completed a plate appearance since the error.
				\item Continuation of play if another batter has completed a plate appearance since the error.
			\end{deepEnumerate}
			\item The League Operation Managers may make a different determination of how play shall proceed.
		\end{deepEnumerate}
	\end{deepEnumerate}
	\item Auto timers
	\begin{deepEnumerate}
		\item Both pitchers and batters are subject to an auto timer from the time they are pinged on reddit to the time they submit a number.
		\item The auto timer is 12 hours long.
		\begin{deepEnumerate}
			\item For example, a batter that is summoned for a plate appearance at 9AM has until 9PM to submit a number.
		\end{deepEnumerate}
		\item Players must submit a number prior to the expiration of the auto timer.
		\item If a player fails to submit a number prior to the expiration of the auto timer, an auto is issued.
		\begin{deepEnumerate}
			\item Any numbers submitted after the expiration of the auto timer are invalid and will be ignored.
			\item Batters issued an auto automatically strike out. This strikeout is credited to the batter and pitcher but officially counts as an automatic strikeout.
			The pitch number is not revealed if the batter autos.
			\item Pitchers issued an auto automatically walk the current batter. This walk is credited to the batter and pitcher but officially counts as an automatic walk.
		\end{deepEnumerate}
		\item Any player who is issued two consecutive autos must be subbed out.
		\item Delays of game
		\begin{deepEnumerate}
			\item Warnings for delay of game shall be issued to a team after 3 total autos by that team.
			\item 5 total autos shall result in automatic forfeiture of the game by that team.
			\item A team purposely delaying a game shall result in automatic forfeiture of the game by that team.
			\begin{deepEnumerate}
				\item Teams forfeiting a game this way are subject to a 9-hour auto timer for the game following the forfeiture.
			\end{deepEnumerate}
			\item Intentional delay of a plate appearance by a player will result in the ejection of the player.
			\begin{deepEnumerate}
				\item This must only be enforced if evidence is provided that the player admitted to the delay, or is very obviously ignoring Reddit and Discord mentions.
			\end{deepEnumerate}
		\end{deepEnumerate}
		\item If a player is ejected due to autos or intentional delay and there are no eligible substitutions, see \hyperref[sec:lineups]{Section 2.3.1}.
		\item The auto timer may be paused for certain situations.
		\begin{deepEnumerate}
			\item Pinch hitters and pitcher substitutions automatically pause the timer (see \hyperref[sec:substitutions]{Substitutions}).
			\item The League Operation Managers may also pause auto timers leaguewide for special events such as holidays and for reddit outages.
			Players and GMs should keep an eye on Discord announcements for these cases.
		\end{deepEnumerate}
	\end{deepEnumerate}
	\item Extra innings
	\label{sec:extra innings}
	\begin{deepEnumerate}
		\item Teams begin each extra inning with baserunners according to the following table.
		\begin{center}										                
			\begin{longtable}{|p{3cm}|p{8cm}|}
				\hline
				\textbf{Inning} & \textbf{Beginning situation}                                          \\
				\hline
				7th             & The player who batted last in the 6th inning is placed on second.     \\
				\hline
				8th             & The player who batted last in the 7th inning is placed on second.     
				The player who batted second-to-last in the 7th inning is placed on third. \\
				\hline
				9th or later    & The player who batted last in the previous inning is placed on first. 
				The player who batted second-to-last is placed on second.
				The player who batted third-to-last is placed on third. \\
				\hline
			\end{longtable}
		\end{center}
		\item Runners who begin an inning on base shall not be credited with a run if they score and
		do not count as earned to the pitcher.
		\item In extra innings, umpires can issue autos starting after 6 hours and must issue autos after 12 hours.
	\end{deepEnumerate}
	\item Substitutions
	\label{sec:substitutions}
	\begin{deepEnumerate}
		\item GMs and team captains are responsible for making substitutions during the course of the game.
		All substitutions default to MLB rules for lineup rules, including the DH rule.
		\begin{deepEnumerate}
			\item For reference, the most common and important rules are as follows:
			\begin{deepEnumerate}
				\item Players who exit the game may not be substituted back into the game in any capacity.
				\item Players may not move between slots in the batting order.
				\item If a DH is moved to any fielding position, the team cannot use a DH for the rest of the game. 
				The pitcher bats in the slot of the player who is replaced in the field by the DH.
				\item Only players who are not already in the game may sub in at DH.
			\end{deepEnumerate}
		\end{deepEnumerate}
		\item Pitching changes must be announced in the game thread in a top-level comment.  
		This can be done as either its own comment or within the first plate appearance the pitcher pitches.
		\item Pinch hitters and runners must be presented to the pitcher prior to a plate appearance. 
		The pitcher shall be given a reasonable opportunity to change their pitch in the event of a pinch hitter or runner.
		\begin{deepEnumerate}
			\item If an umpire presents a pinch hitter or runner to the current pitcher, any further changes regarding that pinch hitter or runner prior to the announcement 
			(e.g. changing which player will pinch hit) must also be presented to the current pitcher.
			\item This does not apply to list pitchers. It is highly recommended to ask list pitchers how they want to handle pinch hitters and runners before they begin pitching.
			\item The auto timer for the plate appearance pauses when the umpire is notified of the substitution by the GM/Captain, 
			regardless of when the umpire responds to it. The auto timer resumes when the pitcher confirms the pitch.
			\begin{deepEnumerate}
				\item As a courtesy to avoid confusion, umpires should post in the new plate appearance how much time is remaining before an auto.
			\end{deepEnumerate}
		\end{deepEnumerate}
		\item Substitutions are official and final immediately upon being sent to the umpire by the GM or acting GM.
		\begin{deepEnumerate}
			\item Substitutions are made by either:
			\begin{deepEnumerate}
				\item Sending a message to all of the involved umps, or 
				\item Posting a top-level comment in the game thread.
			\end{deepEnumerate}
			\item If neither of the above are met, the substitution will not be considered official and will not occur.
			\item A GM can request substitutions to be made if one or more conditions are met.
			\begin{deepEnumerate}
				\item Conditional substiutions shall be considered official and final immediately upon all their conditions being met. 
				\item Conditional substitutions may be cancelled by the GM before the conditions are met upon explicit request to cancel.
			\end{deepEnumerate}
		\end{deepEnumerate}
	\end{deepEnumerate}
	\item Ejections
	\begin{deepEnumerate}
		\item GMs or players may be ejected from the game by an umpire.
		\begin{deepEnumerate}
			\item Ejected GMs must choose a player to take their place. If no choice is made, the primary team captain will be automatically designated.
			\item Ejected players must be replaced with a valid substitution.
			\item Ejected persons should refrain from commenting in the game thread.
		\end{deepEnumerate}
	\end{deepEnumerate}
\end{deepEnumerate}
